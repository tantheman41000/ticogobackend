package dv.ticobackend.demo.config

import dv.ticobackend.demo.entity.*
import dv.ticobackend.demo.repository.*

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.stereotype.Component
import java.sql.Timestamp
import javax.transaction.Transactional

@Component
class ApplicationLoader : ApplicationRunner {
    @Autowired
    lateinit var movieRepository: MovieRepository
    @Autowired
    lateinit var cinemaRepository: CinemaRepository
    @Autowired
    lateinit var seatRepository: SeatRepository
    @Autowired
    lateinit var seatPositionRepository: SeatpositionRepository
    @Autowired
    lateinit var showTimeRepository: ShowTimeRepository
    @Autowired
    lateinit var userRepository: UserRepository

    @Transactional
    fun GenSeat(type: String, row: Int, column: Int): MutableList<SeatPosition>? {
        var seatList = mutableListOf<SeatPosition>()
        for (i in 1..row) {
            for (j in 1..column) {
                var seat = seatPositionRepository.save(SeatPosition(SeatStatus.AVIALABLE, i, j, type))
                seatList.add(seat)
            }
        }
        return seatList;
    }


   fun GenShowtime(seatLevel : MutableList<Seat>):MutableList<Seat> {
       var seatLList = mutableListOf<Seat>()
       for(eachlevel in seatLevel){
           var seatL1 = seatRepository.save(eachlevel)
           seatL1.rowList = GenSeat(eachlevel.name!!, eachlevel.row!!, eachlevel.column!!)
           seatLList.add(seatL1)
       }
       return seatLList
    }
    @Transactional
    override fun run(args: ApplicationArguments?) {
        var user1 = userRepository.save(User("Tan","tanrsd@gmail.com",UserStatus.ACTIVE))
        var mov1 = movieRepository.save(Movie("Alita: Bettle angel", 122, "https://upload.wikimedia.org/wikipedia/en/e/ee/Alita_Battle_Angel_%282019_poster%29.png"))
        var mov2 = movieRepository.save(Movie("Thor: Ragnarok", 130, "https://m.media-amazon.com/images/M/MV5BMjMyNDkzMzI1OF5BMl5BanBnXkFtZTgwODcxODg5MjI@._V1_UX182_CR0,0,182,268_AL_.jpg"))
        var mov3 = movieRepository.save(Movie("อภินิหารไวกิ้งพิชิตมังกร 3", 105, "https://lh3.googleusercontent.com/xTpxPyQOfsnpA2lkp8Fz7LjVA-hsadGF0U7c5rwJq4ikRG_np-5_kO-gzHMVkur2Y5BnLQ4Sfki_Qj7T0Mlisw=w1024"))
        var mov4 = movieRepository.save(Movie("Captain Marvel", 130, "https://lh3.googleusercontent.com/3Ost-FvXYMVAs84hLxJAn9qqIh9s_YCC8oCySCuOWIJWIu8zPIuKYJ9FG_FEFBqKZoQnguVOuNAkg9kArkPM=w1024"))
        var mov5 = movieRepository.save(Movie("เฟรนด์โซน ระวัง..สิ้นสุดทางเพื่อน", 120, "https://lh3.googleusercontent.com/hGX4-13iqHM1Kx_nyj67AtCsFXAjJyS0wxoEcJynOs99BUJXQWLIUQFcsje3jdMW75wtrJU2ktywEF3gxoKe=w1024"))

        var cine1 = cinemaRepository.save(Cinema("A"))
        var cine2 = cinemaRepository.save(Cinema("B"))
        var cine3 = cinemaRepository.save(Cinema("C"))
        var cine4 = cinemaRepository.save(Cinema("D"))
        var cine5 = cinemaRepository.save(Cinema("E"))
        var seatLevell = Seat("sofa", 250.50, 6, 5)
        var seatLevel2 = Seat("deluxe", 500.00, 6, 6)
        var seatLevel3 = Seat("premium", 700.00, 6, 4)
        var seat1 = seatRepository.save(seatLevell)
        var seat2 = seatRepository.save(seatLevel2)
        var seat3 = seatRepository.save(seatLevel3)
        val showtimeCinema1 = showTimeRepository.save(ShowTime(
                Timestamp(1553450210).time, Timestamp(1553457360).time, "en", "n/a"))
        val showtimeCinema2 = showTimeRepository.save(ShowTime(
                Timestamp(1553450210).time, Timestamp(1553457360).time, "th", "n/a"))
        val showtimeCinema3 = showTimeRepository.save(ShowTime(
                Timestamp(1553450210).time, Timestamp(1553457360).time, "en", "th"))
        val showtimeCinema4 = showTimeRepository.save(ShowTime(
                Timestamp(1553450210).time, Timestamp(1553457360).time, "th", "en"))
        val showtimeCinema5 = showTimeRepository.save(ShowTime(
                Timestamp(1553450210).time, Timestamp(1553457360).time, "cam", "en"))

        cine1.movie = mov1
        mov1.showtime!!.add(showtimeCinema1)
        mov1.showtime!!.add(showtimeCinema3)
        showtimeCinema1.seat= GenShowtime(mutableListOf(seatLevell,seatLevel2,seatLevel3) )

        cine2.movie = mov2
        mov2.showtime!!.add(showtimeCinema2)
        showtimeCinema2.seat= GenShowtime(mutableListOf(seatLevell,seatLevel3))

        cine3.movie = mov3
        mov3.showtime!!.add(showtimeCinema3)
        showtimeCinema3.seat=GenShowtime(mutableListOf(seatLevell,seatLevel2,seatLevel3))

        cine4.movie = mov4
        mov3.showtime!!.add(showtimeCinema4)
        showtimeCinema4.seat = GenShowtime(mutableListOf(seatLevell,seatLevel2,seatLevel3))

        cine5.movie = mov5
        mov5.showtime!!.add(showtimeCinema5)
        showtimeCinema5.seat = GenShowtime(mutableListOf(seatLevell,seatLevel2,seatLevel3))




    }
}