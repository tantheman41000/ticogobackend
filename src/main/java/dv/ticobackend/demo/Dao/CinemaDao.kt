package dv.ticobackend.demo.Dao

import dv.ticobackend.demo.entity.Cinema

interface CinemaDao{
    fun getAllCinema() : List<Cinema>
    fun getCinemaByname(name: String): Cinema?
}