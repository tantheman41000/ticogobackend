package dv.ticobackend.demo.Dao

import dv.ticobackend.demo.entity.ShowTime

interface ShowTimeDao{
    fun getShowtime(): List<ShowTime>

}