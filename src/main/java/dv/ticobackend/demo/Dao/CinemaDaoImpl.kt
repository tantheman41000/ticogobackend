package dv.ticobackend.demo.Dao

import dv.ticobackend.demo.entity.Cinema
import dv.ticobackend.demo.entity.Movie
import dv.ticobackend.demo.repository.CinemaRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Repository

@Repository
class CinemaDaoImpl: CinemaDao{


    override fun getCinemaByname(name: String): Cinema? {
        return cinemaRepository.findByName(name)
    }

    @Autowired
    lateinit var cinemaRepository : CinemaRepository

    override fun getAllCinema(): List<Cinema> {
        return cinemaRepository.findAll().filterIsInstance(Cinema::class.java)
    }


}