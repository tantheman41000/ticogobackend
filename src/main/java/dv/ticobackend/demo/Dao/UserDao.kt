package dv.ticobackend.demo.Dao

import dv.ticobackend.demo.entity.User


interface UserDao
{
    fun save(user: User): User?
    fun getAllUser(): List<User>
//    fun getUserById(id: Long) : User?
}