package dv.ticobackend.demo.Dao

import dv.ticobackend.demo.entity.Seat
import dv.ticobackend.demo.repository.SeatRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Repository


@Repository
class SeatDaoImpl : SeatDao{
    @Autowired
    lateinit var seatRepository: SeatRepository
    override fun getAllSeat(): List<Seat> {
       return seatRepository.findAll().filterIsInstance(Seat::class.java)
    }
}
