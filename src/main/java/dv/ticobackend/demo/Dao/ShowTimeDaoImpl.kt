package dv.ticobackend.demo.Dao


import dv.ticobackend.demo.entity.ShowTime
import dv.ticobackend.demo.repository.ShowTimeRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Repository
import java.util.*

@Repository
class ShowTimeDaoImpl: ShowTimeDao{




    @Autowired
    lateinit var showTimeRepository: ShowTimeRepository
    override fun getShowtime(): List<ShowTime> {
        return showTimeRepository.findAll().filterIsInstance(ShowTime::class.java)
    }


}