package dv.ticobackend.demo.Dao

import dv.ticobackend.demo.entity.Movie


interface MovieDao{
    fun GetAllMovie(): List<Movie>
    fun getShowtimeById(id: Long) : Movie?
}