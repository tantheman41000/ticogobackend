package dv.ticobackend.demo.Dao

import dv.ticobackend.demo.entity.Seat

interface SeatDao{
    fun getAllSeat() : List<Seat>
}