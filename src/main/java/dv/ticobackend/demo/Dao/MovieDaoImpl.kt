package dv.ticobackend.demo.Dao

import dv.ticobackend.demo.entity.Movie
import dv.ticobackend.demo.repository.MovieRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Repository

@Repository
class MovieDaoImpl : MovieDao{
    override fun getShowtimeById(id: Long): Movie? {
        return movieRepository.findByShowtimeId(id)
    }

    @Autowired
    lateinit var movieRepository: MovieRepository
    override fun GetAllMovie(): List<Movie> {
        return movieRepository.findAll().filterIsInstance(Movie::class.java)
    }


}