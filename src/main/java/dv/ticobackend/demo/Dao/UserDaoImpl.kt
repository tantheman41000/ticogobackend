package dv.ticobackend.demo.Dao


import dv.ticobackend.demo.entity.User
import dv.ticobackend.demo.repository.UserRepository

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Repository

@Repository
class UserDaoImpl: UserDao{

//    override fun getUserById(id: Long): User? {
//        return userRepository.findByUserId(id)
//    }

    override fun getAllUser(): List<User> {
        return userRepository.findAll().filterIsInstance(User::class.java)
    }

    @Autowired
    lateinit var userRepository: UserRepository
    override fun save(user: User): User? {
        return userRepository.save(user)
    }

}