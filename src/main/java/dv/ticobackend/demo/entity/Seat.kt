package dv.ticobackend.demo.entity

import com.mysql.cj.result.BufferedRowList
import javax.persistence.*


@Entity
data class Seat(
    var name: String? = null,
    var price: Double? = null,
    @Column(name = "seatedColumn")
    var column : Int? = null,
    @Column(name = "seatedRow")
    var row: Int? = null,
    @OneToMany
    var rowList: MutableList<SeatPosition>? = mutableListOf()

){
    @Id
    @GeneratedValue
    var id:Long? = null
}