package dv.ticobackend.demoentity.entity

import java.util.Date

data class Date
    (
    var startShowDate : Date? = null,
    var endShowDate : Date? = null

)