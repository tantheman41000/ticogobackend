package dv.ticobackend.demo.entity

import javax.persistence.*


@Entity
data class Movie(
    var name:String? = null,
    var duration : Int? = null,
    var imageMovieUrl: String?= null


){
    @Id
    @GeneratedValue
    var id:Long? = null

    @ManyToMany
    var showtime = mutableListOf<ShowTime>()

}

