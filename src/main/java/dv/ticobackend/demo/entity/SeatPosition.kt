package dv.ticobackend.demo.entity

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
data class SeatPosition(
    var seatStatus: SeatStatus? = SeatStatus.AVIALABLE,
    @Column(name = "seatRow")
    var row : Int? =null,
    @Column(name = "seatColumn")
    var column: Int? = null,
    var type: String? = null

){
    @Id
    @GeneratedValue
    var id:Long? = null
}


