package dv.ticobackend.demo.entity

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
data class User (

        var name:String?=null,
        var email:String?=null,
        var userStatus: UserStatus?
){
    @Id
    @GeneratedValue
    var id:Long? = null
}

