package dv.ticobackend.demo.entity

import java.util.*
import javax.persistence.*


@Entity
data class ShowTime(

    var showStart : Long? = null,
    var ShowEnd : Long? = null,

    var soundtrack:String?= null,
    var subtitle : String? = null


){
    @Id
    @GeneratedValue
    var id:Long? = null

    @ManyToMany
    var seat = mutableListOf<Seat>()

}