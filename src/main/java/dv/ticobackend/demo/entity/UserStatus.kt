package dv.ticobackend.demo.entity

enum class UserStatus{
    PENDING,ACTIVE,NOACTIVE,DELETED
}