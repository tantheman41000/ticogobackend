package dv.ticobackend.demo.service

import dv.ticobackend.demo.Dao.UserDao
import dv.ticobackend.demo.entity.User
import dv.ticobackend.demo.entity.dto.UserDto
import dv.ticobackend.demo.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class UserServiceImpl: UserService{

//    override fun getUserById(id: Long): User? {
//        return userDao.getUserById(id)
//    }

    override fun getAllUser(): List<User> {
        return userDao.getAllUser()
    }

    @Autowired
    lateinit var userDao: UserDao

    override fun save(user: User): User? {
        return userDao.save(user)
    }


}