package dv.ticobackend.demo.service

import dv.ticobackend.demo.entity.Movie

import javax.persistence.Id


interface MovieService{
    fun getAllMovie(): List<Movie>
    fun getShowtimeById(id: Long): Movie?
}