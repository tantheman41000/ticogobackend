package dv.ticobackend.demo.service

import dv.ticobackend.demo.entity.Cinema


interface CinemaService{
    fun getAllCinema(): List<Cinema>
    fun getCinemaByname(name:String): Cinema?
}
