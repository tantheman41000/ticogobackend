package dv.ticobackend.demo.service

import dv.ticobackend.demo.Dao.ShowTimeDao
import dv.ticobackend.demo.entity.ShowTime
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class ShowTimeServiceImpl: ShowTimeService{
    override fun getShowtime(): Any {
        return showTimeDao.getShowtime()
    }


    @Autowired
    lateinit var showTimeDao : ShowTimeDao


}