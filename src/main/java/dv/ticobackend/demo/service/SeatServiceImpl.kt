package dv.ticobackend.demo.service

import dv.ticobackend.demo.Dao.SeatDao
import dv.ticobackend.demo.entity.Movie
import dv.ticobackend.demo.entity.Seat
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class SeatServiceImpl: SeatService{
    @Autowired
    lateinit var seatDao : SeatDao
    override fun getAllSeat(): List<Seat> {
        return seatDao.getAllSeat()
    }
}