package dv.ticobackend.demo.service

import dv.ticobackend.demo.entity.User

interface UserService{
    fun save(user: User): User?
    fun getAllUser(): List<User>
//     fun getUserById(id: Long): User?
}