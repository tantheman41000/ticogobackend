package dv.ticobackend.demo.service

import dv.ticobackend.demo.entity.Seat

interface SeatService{
    fun getAllSeat() : List<Seat>
}