package dv.ticobackend.demo.service

import dv.ticobackend.demo.Dao.CinemaDao
import dv.ticobackend.demo.Dao.MovieDao
import dv.ticobackend.demo.entity.Cinema
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class CinemaServiceImpl : CinemaService{


    @Autowired
    lateinit var cinemaDao: CinemaDao
    override fun getAllCinema(): List<Cinema> {
        return cinemaDao.getAllCinema()
    }
    override fun getCinemaByname(name:String): Cinema? {
        return cinemaDao.getCinemaByname(name)
    }
}