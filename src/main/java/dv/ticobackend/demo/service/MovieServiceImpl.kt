package dv.ticobackend.demo.service

import dv.ticobackend.demo.Dao.MovieDao
import dv.ticobackend.demo.entity.Movie


import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class MovieServiceImpl : MovieService{



    override fun getShowtimeById(id: Long): Movie? {
        return movieDao.getShowtimeById(id)
    }


    @Autowired
    lateinit var movieDao: MovieDao
    override fun getAllMovie(): List<Movie> {
       return movieDao.GetAllMovie()
    }

}