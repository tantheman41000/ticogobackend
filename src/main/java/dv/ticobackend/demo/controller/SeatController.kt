package dv.ticobackend.demo.controller

import dv.ticobackend.demo.service.MovieService
import dv.ticobackend.demo.service.SeatService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class SeatController{

    @Autowired
    lateinit var seatService : SeatService

    @GetMapping("/seats")
    fun getAllMovie(): ResponseEntity<Any> {
        return ResponseEntity.ok(seatService.getAllSeat())
    }


}