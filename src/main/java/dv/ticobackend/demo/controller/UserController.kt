package dv.ticobackend.demo.controller

import dv.ticobackend.demo.entity.User
import dv.ticobackend.demo.entity.dto.UserDto
import dv.ticobackend.demo.service.UserService
import dv.ticobackend.demo.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*


@RestController
class UserController{
    @Autowired
    lateinit var userService: UserService

    @PostMapping("/user")
    fun addUser(@RequestBody user: User) : ResponseEntity<Any> {
        val output = userService.save(user)
        output?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

    @GetMapping("/user")
    fun getAllUser(): ResponseEntity<Any>{
        return ResponseEntity.ok(userService.getAllUser())
    }

//    @GetMapping("/user/")
//    fun getUserById(@RequestParam("userId")user: Long) : ResponseEntity<Any>{
//        var output = userService.getUserById(user)
//        output?.let { return ResponseEntity.ok(it) }
//        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
//    }

}