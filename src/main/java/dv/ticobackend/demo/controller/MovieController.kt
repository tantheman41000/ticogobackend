package dv.ticobackend.demo.controller

import dv.ticobackend.demo.service.MovieService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import javax.persistence.Id

@RestController
class MovieController{

    @Autowired
    lateinit var movieService : MovieService

    @GetMapping("/movies")
    fun getAllMovie(): ResponseEntity<Any> {
        return ResponseEntity.ok(movieService.getAllMovie())
    }

    @GetMapping("/movie/showtime/")
    fun getShowtimeById(@RequestParam("showtimeId")showtime: Long): ResponseEntity<Any>{
        var output = movieService.getShowtimeById(showtime)
        output?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }


}

