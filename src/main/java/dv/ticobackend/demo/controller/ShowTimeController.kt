package dv.ticobackend.demo.controller


import dv.ticobackend.demo.service.ShowTimeService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
class ShowTimeController{
    @Autowired
    lateinit var showTimeService : ShowTimeService


    @GetMapping("/showtimes")
    fun getShowtimeById(): ResponseEntity<Any> {
        return ResponseEntity.ok(showTimeService.getShowtime())
    }




    //getshowtimebyMovie
    //getoneshowtimeby

}