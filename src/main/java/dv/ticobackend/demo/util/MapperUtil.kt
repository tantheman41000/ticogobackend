package dv.ticobackend.demo.util

import dv.ticobackend.demo.entity.Cinema
import dv.ticobackend.demo.entity.User
import dv.ticobackend.demo.entity.dto.CinemaDto
import org.mapstruct.Mapper

import org.mapstruct.factory.Mappers


@Mapper(componentModel = "spring")
interface MapperUtil {
    companion object {
        val INSTANCE = Mappers.getMapper(MapperUtil::class.java)!!
    }


    fun mapCinemaDto(cinema: Cinema?): CinemaDto?


}