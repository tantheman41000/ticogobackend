package dv.ticobackend.demo.repository

import dv.ticobackend.demo.entity.ShowTime
import org.springframework.data.repository.CrudRepository

interface ShowTimeRepository: CrudRepository<ShowTime, Long> {

}