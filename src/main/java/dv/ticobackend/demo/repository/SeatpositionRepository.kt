package dv.ticobackend.demo.repository

import dv.ticobackend.demo.entity.SeatPosition
import org.springframework.data.repository.CrudRepository

interface SeatpositionRepository: CrudRepository<SeatPosition, Long> {

}