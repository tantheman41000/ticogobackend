package dv.ticobackend.demo.repository


import dv.ticobackend.demo.entity.Seat
import org.springframework.data.repository.CrudRepository

interface SeatRepository: CrudRepository<Seat, Long> {

}