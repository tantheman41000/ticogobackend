package dv.ticobackend.demo.repository

import dv.ticobackend.demo.entity.Cinema
import org.springframework.data.repository.CrudRepository


interface CinemaRepository: CrudRepository<Cinema, Long> {
    abstract fun findByName(name: String): Cinema?

}