package dv.ticobackend.demo.repository

import dv.ticobackend.demo.entity.Movie
import org.springframework.data.repository.CrudRepository


interface MovieRepository:CrudRepository<Movie,Long>{
  fun findByShowtimeId(id: Long): Movie?
}