package dv.ticobackend.demo.repository


import dv.ticobackend.demo.entity.User
import org.springframework.data.repository.CrudRepository

interface UserRepository:CrudRepository<User,Long>{
//     fun findByUserId(id: Long) : User?
}